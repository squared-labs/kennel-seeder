#!/bin/bash
set -e
echo "This script should be run with the project as the working dir."
# just in case laravel breaks the path
pushd .
composer create-project --prefer-dist laravel/laravel "tmp"
popd
rsync -a -v --ignore-existing tmp/ "`pwd`"
rm -r "tmp"
echo -e "\nLaravel should be installed. Follow the instructions in the README."
