<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateHuskiesTable extends Migration
{
   /**
    * Run the migrations.
    *
    * @return void
    */
   public function up()
   {
       Schema::create('huskies', function (Blueprint $table) {

           $table->increments('id');
           $table->string('netid')->unique();
           $table->string('last_name');
           $table->string('first_name');
           $table->string('work_title');
           $table->string('group_affil');
           $table->string('dept_id');
           $table->string('dept_desc');
           $table->date('hire_date')->nullable();
       });
   }

   /**
    * Reverse the migrations.
    *
    * @return void
    */
   public function down()
   {
       Schema::drop('huskies');
   }
}
