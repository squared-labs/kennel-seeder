<?php

namespace KennelSeeder;

use Illuminate\Support\ServiceProvider;

class KennelSeederServiceProvider extends ServiceProvider
{
    public function boot()
    {
        $this->loadMigrationsFrom(__DIR__.'/../migrations');
    }
}
