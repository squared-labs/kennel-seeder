<?php

namespace KennelSeeder;

use Illuminate\Database\Seeder;
// For Eloquent call
use KennelSeeder\Husky;

class BaseHuskySeeder extends BaseSeeder
{
    /**
     * List of fields that can be used to determine the uniqueness of a row in $tableName
     * @see rowAlreadyExists
     */
    protected $tableUniqueFields = ['netid'];

    protected $arrayFieldToDbField = [
        "first_name" => 'first_name',
        "last_name" => 'last_name',
        "netid" => 'netid',
        "group_affil" => 'group_affil',
        "work_title" => 'work_title',
        "dept_id" => 'dept_id',
        "dept_desc" => 'dept_desc',
        "hire_date" => 'hire_date',
    ];

    /**
     * Processes a row of the CSV seed file. The data will be mapped to DB field names and sanitized.
     * Rows which pass filter criteria (@see acceptRow, filterFuncByDbField) will be inserted into the DB
     */
    protected function processRow($row)
    {
        $rowData = [];
        collect($this->arrayFieldToDbField)->each( function ($dbField, $arrayField) use ($row, &$rowData) {
            $rowData[$dbField] = $row[$arrayField];
        });

        // http://stackoverflow.com/a/27479955
        Husky::updateOrCreate(['netid' => $rowData['netid']], [
            'first_name' => $rowData['first_name'],
            'last_name' => $rowData['last_name'],
            'group_affil' => $rowData['group_affil'],
            'work_title' => $rowData['work_title'],
            'dept_id' => $rowData['dept_id'],
            'dept_desc' => $rowData['dept_desc'],
            'hire_date' => $rowData['hire_date'],
        ]);

    }

    /*
    * The filter functions sanitizeRowData, acceptRow, and rowAlreadyExists
    * from previous implementations of seeder logic may be unhooked.
    * This is because we always want to upsert rows, and we are
    * (or should be) guaranteed safe rows from Kennel
    */

}
