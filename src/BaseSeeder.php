<?php

namespace KennelSeeder;

use Illuminate\Database\Seeder;

class BaseSeeder extends \Illuminate\Database\Seeder
{
    // The converter function csvToArray will not be used in this application
    // because we rely on an api call for data instead of importing from a file.
    // Very modern! :)
    // However, the log function is left here, should it come in handy at some point in the future

    protected $debugPrint = true;

    protected function log($msg, $level='debug')
    {
        if ($this->debugPrint) {
            print $msg;
        }
    }
}
