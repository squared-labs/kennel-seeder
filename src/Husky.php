<?php

namespace KennelSeeder;

use Illuminate\Database\Eloquent\Model;

/**
 * Husky = a sorter term for "UConn Person" - it encapsulates UConn faculty,
 * staff, students, etc. (basically anyone with a NetID is a Husky)
 */
class Husky extends Model
{
    public $timestamps = false;

    protected $casts = [
        'hire_date' => 'date',
    ];
}
