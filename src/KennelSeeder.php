<?php

namespace KennelSeeder;

use Illuminate\Database\Seeder;
use GuzzleHttp\Exception\GuzzleException;
use GuzzleHttp\Client;

class KennelSeeder extends \KennelSeeder\BaseHuskySeeder
{
    /**
     * Populates the Husky Table with the json response from kennel
     *
     * @return void
     */
    public function run()
    {
        $client = new Client([
            // Base URI is used with relative requests
            'base_uri' => config('services.kennel.url'),
            // The kennel API looks for a header called Authorization
            // You can find that spec in kennel's middleware file QDKeyAuth.php
            // We also set up the token to be on a client instance, since a
            // client website uses one and the same token
            'headers' => ['Authorization' => config('services.kennel.token')],
        ]);

        // Additional path (2nd param) is relative to the base url. See services.php comment for more info!
        // We make GET requests because count and export routes are defined as a GET requests in kennel
        $countrequest = $client->request('GET', 'count');

        // http://php.net/manual/en/function.json-decode.php
        // The reason why we want an associate array is because the driving seeder
        // i.e. BaseFacultySeeder takes in an array
        $deep_assoc_transform = true;
        $countresponse = json_decode($countrequest->getBody(), $deep_assoc_transform);
        $numberpages = $countresponse['pages'];

        for($i=1; $i<=$numberpages; $i++) {
            // Get me back one "page's" worth of records at a time from Kennel
            $exportrequest = $client->request('GET', 'export', ['query' => ['page' => $i]]);
            $exportedreponse = json_decode($exportrequest->getBody(), $deep_assoc_transform);

            // The exportedreponse from the api call contains 500 rows of data!
            foreach ($exportedreponse as $row) {
                $this->processRow($row);
            }
            print("imported page ".$i." of ".$numberpages." (".(int)(100*$i/$numberpages)."%)\r");
        }

        /*
        * The global variables santizeFuncByDbField and filterFuncByDbField
        * from previous implementations of seeder logic are not needed in this
        * application. This is because we always want to upsert rows, and we are
        * (or should be) guaranteed safe rows from Kennel
        */

    }
}
