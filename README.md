# kennel-seeder
Provide your projects with fresh user data from the UConn Kennel!

## Setting up your development environment
```
docker-compose up -d # start containers
. scripts/attach.sh # attach to workspace container

# inside container:
composer install # pull in packages
```

## Setting up kennel-seeder on a project
Run `composer require uconn-core/kennel-seeder`.

Then add `KennelSeeder\KennelServiceProvider` to your providers list in your config/app.php file.

Also, make sure to set the URL and API key in your .env files.

If you're having trouble, see [https://gitlab.com/uconn-core/faculty-travel/merge_requests/7/commits], [https://gitlab.com/uconn-core/faculty-travel/merge_requests/8/commits], and [https://gitlab.com/uconn-core/faculty-travel/blob/8424743884691a30a3db8fa27d935125cace853c/.env.example#L35-36]

Also please read the Kennel docs.
