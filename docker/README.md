# docker

This directory holds image configuration, data, and logs. It is forked from
[LaraDock](https://github.com/LaraDock/laradock) at commit
27d352546931a551077572c8c7ccd788e804b2d9.

The fork removes services unnecessary to this project and adds elasticsearch.
