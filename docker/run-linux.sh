#!/bin/bash

# This is necessary so boot2docker doesn't create the folders later with root
# ownership.

DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"

mkdir -p ${DIR}/nginx/log
mkdir -p ${DIR}/mysql/data
mkdir -p ${DIR}/mysql/log

# $UID is a shell variable that docker-compose.linux.yml needs. Exporting it as
# an environment variable so docker-compose.linux.yml can see it.
export UID
docker-compose -f docker-compose.yml -f docker-compose.linux.yml up "$@"
